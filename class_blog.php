<?php



class DB {

    private $host = 'localhost';
    private $db_name = 'blog';
    private $username = 'user';
    private $password = 'azerty123';
    private $conn;
  
    public function connect() {
      $this->conn = null;
      try {
        $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      } catch(PDOException $e) {
        echo 'Connection Error: ' . $e->getMessage();
      }
  
      return $this->conn;
    }
}
  
class Comment {

    public $author;
    public $content;
    public $id;
  
    // Constructeur de la classe
    public function __construct($author, $content, $id) {
      $this->author = $author;
      $this->content = $content;
      $this->id = $id;
    }

        public function save1() {
          // Connexion à la base de données
          $db = new PDO('mysql:host=localhost;dbname=blog', 'user', 'azerty123');
      
          // Préparation de la requête d'insertion
          $stmt = $db->prepare("INSERT INTO comment (author, content, articleid) VALUES (?, ?, ?)");
          // Exécution de la requête
          $stmt->execute([$this->author, $this->content, $this->id]);
        }
      
      
  }
  

class Article {

    public $title;
    public $author;
    public $content;
    public $articleid;
    private $conn;
    // private $table = 'articles';
  
    public function __construct($db) {
      $this->conn = $db;
    }
  
    public function save() {
      // Préparation de la requête d'insertion
      $stmt = $this->conn->prepare("INSERT INTO articles (title, author, content) VALUES (?, ?, ?)");
  
      // Exécution de la requête
      $stmt->execute([$this->title, $this->author, $this->content]);
    }
  
    public function getArticles() {
      $query = $this->conn->prepare("SELECT * FROM articles");
      $query->execute();
      return $query->fetchAll();
    }
  }
  
 






// class Article {
//     public $title;
//     public $author;
//     public $content;
  
//     // Constructeur de la classe
//     public function __construct($title, $author, $content) {
//       $this->title = $title;
//       $this->author = $author;
//       $this->content = $content;
//     }


//     public function save() {
//         // Connexion à la base de données
//         $db = new PDO('mysql:host=localhost;dbname=blog', 'user', 'azerty123');
    
//         // Préparation de la requête d'insertion
//         $stmt = $db->prepare("INSERT INTO articles (title, author, content) VALUES (?, ?, ?)");
    
//         // Exécution de la requête
//         $stmt->execute([$this->title, $this->author, $this->content]);
//       }

//       public function getArticles() {

//         $db = new PDO('mysql:host=localhost;dbname=blog', 'user', 'azerty123');
//         $query = $this->$db->prepare("SELECT * FROM articles");
//         $query->execute();
//         return $query->fetchAll();
//       }
// }