<?php
// Récupération des données du formulaire
$title = $_POST['title'];
$author = $_POST['author'];
$content = $_POST['content'];

// Connexion à la base de données
$db = new PDO('mysql:host=localhost;dbname=blog', 'user', 'azerty123');

// Préparation de la requête d'insertion
$stmt = $db->prepare("INSERT INTO articles (title, author, content) VALUES (?, ?, ?)");

// Exécution de la requête
$stmt->execute([$title, $author, $content]);

// Redirection vers la page d'accueil du blog
header('Location: index.php');
exit;
?>
